package com.irises.fs;

import com.irises.fs.local.LocalFileSystem;
import com.irises.fs.oss.AliyunOss;
import com.irises.fs.oss.OssFileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * 文件系统工具类
 * <p/>
 * 该类提供通过 url 来获取文件系统的方法, 文件系统 url 格式如下:<br/>
 * schema://[user[:password]@]/path <br/>
 * eg:<br/>
 * local:///mnt/project <br/>
 * sftp://user:passwd@[/mnt]
 * aliyun://access_key_id:access_key_secure@bucket.endpoint
 * <br />
 * 注: 如果 URI 中存在特殊字符需要使用 {@link java.net.URLEncoder#encode(String, String)} 进行转移,
 * eg: passwd=foo@bar 则 URI 中应该是 foo%40bar.
 *
 * @author tianxuan
 */
public abstract class IrisesFileSystems {
    private static final Logger LOG = LoggerFactory.getLogger(IrisesFileSystems.class);

    /* *****************************************************************
     *                    Get FileSystem
     * *****************************************************************/

    /**
     * 根据给定的文件系统 url 创建文件系统
     *
     * @param url 文件系统 url
     * @return 文件系统实例
     */
    public static IrisesFileSystem getFileSystem(String url) {
        Properties info = new Properties();
        return getFileSystem(url, info);
    }

    public static OssFileSystem getOssFileSystem(String url) {
        Properties info = new Properties();
        return (OssFileSystem) getFileSystem(url, info);
    }

    public static LocalFileSystem getLocalFileSystem(String url) {
        Properties info = new Properties();
        return (LocalFileSystem) getFileSystem(url, info);
    }

    /**
     * 根据给定的文件系统url 和 配置信息获取 文件系统实例
     *
     * @param url  文件系统 url
     * @param info 配置信息
     * @return 文件系统实例
     */
    public static IrisesFileSystem getFileSystem(final String url, final Properties info) {
        try {
            final URI fsUri = new URI(url);
            final String scheme = fsUri.getScheme();
            final String userInfo = fsUri.getUserInfo();
            final String host = fsUri.getHost();
            final String path = fsUri.getPath();

            String username = null;
            String password = null;
            if (null != userInfo) {
                String[] split = userInfo.split(":", 2);
                username = split[0];
                password = 1 < split.length ? split[1] : null;
            }

            final String u = info.getProperty("user");
            final String p = info.getProperty("password");
            username = null != u ? u : username;
            password = null != p ? p : password;


            if ("local".equalsIgnoreCase(scheme) || "file".equalsIgnoreCase(scheme)) {
                // local:///path
                assertNotBlank(path, "path must be not blank ('" + url + "').");
                return new LocalFileSystem(path);
            }

            assertNotBlank(host, "host must be not blank ('" + url + "').");

            if ("aliyun".equalsIgnoreCase(scheme)) {
                // aliyun://access_key_id:access_key_secure@bucket.host
                final int pos = host.indexOf('.');
                if (1 > pos) {
                    throw new IllegalArgumentException("No bucket found for '" + url + '\'');
                }

                final String bucketName = host.substring(0, pos);
                final String endpoint = host.substring(pos + 1);

                assertNotBlank(endpoint, "endpoint must be not blank ('" + url + "').");
                assertNotBlank(bucketName, "bucket must be not blank ('" + url + "').");
                assertNotBlank(username, "access key id not found, from '" + url + "' or '" + info + "'.");
                assertNotBlank(password, "access key secure not found, from '" + url + "' or '" + info + "'.");

                return new AliyunOss(endpoint, username, password, bucketName);
            }

            throw new IllegalArgumentException("schema is unsupported: " + scheme);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private static void assertNotBlank(final String text, final String message) {
        if (!hasText(text)) {
            throw new IllegalStateException(message);
        }
    }

    private static boolean hasText(CharSequence charseq) {
        int len;
        if(charseq != null && (len = charseq.length()) != 0) {
            for(int i = 0; i < len; ++i) {
                if(!Character.isWhitespace(charseq.charAt(i))) {
                    return true;
                }
            }

            return false;
        } else {
            return false;
        }
    }

}
