package com.irises.fs.web.api;

import com.irises.freework.web.http.WebResponse;
import com.irises.fs.oss.OssFileSystem;
import com.irises.fs.util.StorageUtil;
import com.irises.fs.web.DownloadView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;


/**
 *   文件上传下载公共接口
 */
@Api(tags = "阿里云文件存储服务")
@Controller
@RequestMapping("/oss")
public class OssStorageAPI implements ServletContextAware {
    public static final String DEFAULT_FILE_MIME_TYPE = "application/octet-stream";
    public static final String DEFAULT_IMAGE_MIME_TYPE = "text/html";
    private ServletContext servletContext;
    @Autowired
    @Qualifier("ossFileSystem")
    private OssFileSystem fileSystem;


    /**********************************************
     *                 文件操作-Begin               *
     * *******************************************/

    /**
     *  通用上传文件
     * @param file
     * @return
     * @throws IOException
     */
    @ApiOperation("上传")
    @ResponseBody
    @PostMapping(value = "/ul")
    public WebResponse upload(@RequestPart("file") MultipartFile file, Integer fileIndex) throws IOException {
        WebResponse message;
        if (file == null || file.isEmpty()) {
            message = WebResponse.fail();

        } else {
            String name = file.getOriginalFilename();
            InputStream inputStream = file.getInputStream();
            String path = StorageUtil.store(fileSystem, name, true, inputStream, true);
            message = WebResponse.success(new HashMap<String, Object>() {{
                put("url",  path);
                put("fileName", name);
                put("fileIndex", fileIndex);
            }});
        }
            return message;
    }


    /**
     * 内联方式获取文件资源
     * @param fid
     * @param request
     * @param response
     * @throws IOException
     */
    @ApiOperation("查看")
    @GetMapping("/{fid:.*}") // spring 默认会去掉后缀, 使用 .* 避免
    public void inline(@PathVariable("fid") String fid, HttpServletRequest request, HttpServletResponse response) throws IOException {
        final InputStream is = StorageUtil.readAsStream(fid,false,fileSystem);
        if (null != is) {
            String mimeType = getMimeType(fid);
            new DownloadView("", mimeType,false) {
                @Override
                protected InputStream getResourceAsStream() {
                    return is;
                }
            }.sendTo(request, response);

        } else {
            // writer.write("您下载的文件不存在")
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "File Not Find");
        }
    }

    /**
     * 文件下载
     * @param fid
     * @param filename
     * @param responseContentType
     * @param request
     * @param response
     * @throws IOException
     */
    @ApiOperation("附件下载")
    @GetMapping("/dl/{fid:.*}")
    // @RequestMapping("dl/{hash}/{filename}")
    public void download(@PathVariable("fid") String fid, String filename, String responseContentType, HttpServletRequest request, HttpServletResponse response) throws IOException {
        final InputStream is = StorageUtil.readAsStream(fid, false,fileSystem);
        if (null != is) {
            filename = StringUtils.hasText(filename) ? filename : fid;
            // response.setHeader("Content-MD5", hash);

            // 如果 contentType 为空, 尝试从 filename 中获取
            if (!StringUtils.hasText(responseContentType)) {
                responseContentType = getMimeType(filename);
            }

            // 如果 contentType 仍然为空, 尝试从标识中获取
            if (!StringUtils.hasText(responseContentType)) {
                responseContentType = getMimeType(fid);
            }
            if (!StringUtils.hasText(responseContentType)) {
                responseContentType = DownloadView.DEFAULT_CONTENT_TYPE;
            }
            responseContentType = StringUtils.hasText(responseContentType) ? responseContentType : DEFAULT_FILE_MIME_TYPE;

            new DownloadView(filename, responseContentType) {
                @Override
                protected InputStream getResourceAsStream() {
                    return is;
                }
            }.sendTo(request, response);
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "File Not Find");
        }
    }


    /**
     * 裁剪图片   /images/aa.jpg_120x120
     */
    @ApiOperation("图片裁剪")
    @GetMapping("images/{path:.*}")
    public void view(@PathVariable("path") String path, boolean download, HttpServletResponse response) throws IOException {
        final InputStream is = StorageUtil.readAsStream(path,true,fileSystem);
        if (null != is) {
            String type = getMimeType(path);
            type = StringUtils.hasText(type) ? type : DEFAULT_IMAGE_MIME_TYPE;

            new DownloadView("", type, download) {
                @Override
                protected InputStream getResourceAsStream() {
                    return is;
                }
            }.sendTo(null, response);
        }
    }

    /**********************************************
     *                 操作-END               *
     * *******************************************/

    private String getMimeType(String filename) {
        return servletContext.getMimeType(filename);
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
}
