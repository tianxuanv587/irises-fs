package com.irises.fs.web.api;

import com.irises.fs.IrisesFileSystem;
import com.irises.fs.util.FilenameUtils;
import com.irises.fs.util.StorageUtil;
import com.irises.fs.web.UEditorConfig;
import com.irises.freework.util.Jackson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@ApiIgnore
@Controller
@RequestMapping({"/fs"})
public class UEditorAPI{

    @Autowired
    @Qualifier("localFileSystem")
    private IrisesFileSystem fileSystem;

    @ResponseBody
    @GetMapping({"ueditor"})
    public String invalid() {
        HashMap<String, Object> rtMap = new HashMap<String, Object>() {{
            put("state", "暂不支持该功能！");
        }};
        return Jackson.toJSONString(rtMap);
    }

    @ResponseBody
    @GetMapping(
            value = {"ueditor"},
            params = {"action=config"}
    )
    public String config(String callback) {
        UEditorConfig config = UEditorConfig.instance();
        return callback + "(" + Jackson.toJSONString(config) +")";
    }

    @ResponseBody
    @PostMapping(
            value = {"ueditor"},
            params = {"action=ul"}
    )
    public String upload(@RequestParam("file") MultipartFile multipart, @RequestParam(value = "relative",required = false,defaultValue = "1") boolean relative, HttpServletRequest request) {
        Map<String, Object> rtMap = new HashMap<>();
        if(null != multipart && !multipart.isEmpty()) {
            try {
                String origName = multipart.getOriginalFilename();
                long size = multipart.getSize();
                String path = StorageUtil.store(fileSystem,origName, true, multipart.getInputStream(),true);
                String suffix = FilenameUtils.getExtension(origName);
                String url = "/" + path;
                if(!relative) {
                    url = request.getContextPath() + url;
                }

                rtMap.put("state", "SUCCESS");
                rtMap.put("name", origName);
                rtMap.put("original", origName);
                rtMap.put("size", Long.valueOf(size));
                rtMap.put("type", suffix);
                rtMap.put("url", url);
            } catch (IOException var11) {
                rtMap.put("state", "Upload Failed");
            }

            return  Jackson.toJSONString(rtMap);
        } else {
            return this.invalid();
        }
    }

}
