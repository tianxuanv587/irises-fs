package com.irises.fs.web.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ResponseData {
    private String url;
    private String fileName;
    private Integer fileIndex;
}
