package com.irises.fs.web;

import lombok.Data;

/**
 * UEditor配置类
 */
@Data
public class UEditorConfig {
    private static UEditorConfig uEditorConfig = new UEditorConfig();
    private static final String prefix = "http://storage.jizhisuma.com:8081";

    private UEditorConfig() {
    }

    public static UEditorConfig instance() {
        return uEditorConfig;
    }

    /* 上传图片配置项 */
    private String imageActionName = "ul";
    private String imageFieldName = "file";
    private int imageMaxSize = 2048000;
    private String[] imageAllowFiles = new String[]{".png", ".jpg", ".jpeg", ".gif", ".bmp"};
    private boolean imageCompressEnable = true;
    private int imageCompressBorder = 1600; /* 图片压缩最长边限制 */
    private String imageUrlPrefix = prefix; /* 图片访问路径前缀 */

    /* 涂鸦图片上传配置项 */
    private String scrawlActionName = "ul"; /* 执行上传涂鸦的action名称 */
    private String scrawlFieldName = "file"; /* 提交的图片表单名称 */
    private int scrawlMaxSize = 2048000;
    private String scrawlUrlPrefix = prefix; /* 图片访问路径前缀 */

    /*截图工具上传*/
    private String snapscreenActionName = "ul"; /* 执行上传截图的action名称 */
    private String snapscreenUrlPrefix = prefix; /* 图片访问路径前缀 */

    /* 抓取远程图片配置 */
    private String[] catcherLocalDomain = new String[]{"img.baidu.com"};
    private String catcherActionName = "ul"; /* 执行抓取远程图片的action名称 */
    private String catcherFieldName = "file"; /* 提交的图片列表表单名称 */
    private String catcherUrlPrefix = prefix; /* 图片访问路径前缀 */
    private int catcherMaxSize = 2048000; /* 上传大小限制，单位B */
    private String[] catcherAllowFiles = new String[]{".png", ".jpg", ".jpeg", ".gif", ".bmp"}; /* 抓取图片格式显示 */

    /* 上传视频配置 */
    private String videoActionName = "ul"; /* 执行上传视频的action名称 */
    private String videoFieldName = "file"; /* 提交的视频表单名称 */
    private String videoUrlPrefix = prefix; /* 视频访问路径前缀 */
    private int videoMaxSize = 102400000;
    private String[] videoAllowFiles = new String[]{
            ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg",
            ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid"}; /* 上传视频格式显示 */

    /* 上传文件配置 */
    private String fileActionName = "ul"; /* controller里,执行上传视频的action名称 */
    private String fileFieldName = "file"; /* 提交的文件表单名称 */
    private String fileUrlPrefix = prefix; /* 文件访问路径前缀 */
    private int fileMaxSize = 51200000; /* 上传大小限制，单位B，默认50MB */
    private String[] fileAllowFiles = new String[]{
            ".png", ".jpg", ".jpeg", ".gif", ".bmp",
            ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg",
            ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid",
            ".rar", ".zip", ".tar", ".gz", ".7z", ".bz2", ".cab", ".iso",
            ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".md", ".xml"
    };
}
