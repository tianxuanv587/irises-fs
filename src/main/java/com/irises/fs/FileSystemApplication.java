package com.irises.fs;

import com.irises.fs.local.LocalFileSystem;
import com.irises.fs.oss.OssFileSystem;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.util.unit.DataSize;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
public class FileSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileSystemApplication.class, args);
    }

    /**
     * fs 模块配置
     * @param environment
     * @return
     */
    @Bean("localFileSystem")
    public LocalFileSystem localFileSystem(Environment environment){
        return IrisesFileSystems.getLocalFileSystem(environment.getProperty("fs.local.url"));
    }

    /**
     * fs 模块配置
     * @param environment
     * @return
     */
    @Bean("ossFileSystem")
    public OssFileSystem ossFileSystem(Environment environment){
        return IrisesFileSystems.getOssFileSystem(environment.getProperty("fs.oss.url"));
    }

    /**
     * 文件上传配置
     * @return
     */
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //单个文件最大
        factory.setMaxFileSize(DataSize.ofMegabytes(64)); //KB,MB
        /// 设置总上传数据总大小
        factory.setMaxRequestSize(DataSize.ofMegabytes(128));
        return factory.createMultipartConfig();
    }
}
