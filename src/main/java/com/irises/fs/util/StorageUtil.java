package com.irises.fs.util;

import com.irises.fs.IrisesFileSystem;
import org.im4java.core.*;
import org.im4java.process.OutputConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class StorageUtil {
    private static final Logger LOG = LoggerFactory.getLogger(StorageUtil.class);
    public static final String SIZE_SEPARATOR = "x";                // 文件尺寸分隔符
    public static final String THUMB_NAME_FMT = "{0}_{1}x{2}.{3}";  // 缩略图命名规则

    private static java.util.List<String> allowSizeList;                             // 允许的缩略图尺寸
    private static boolean storeThumbnail = true;

    /**
     * 存储
     * @param originalFilename
     * @param generateNewName
     * @param in
     * @param override
     * @return
     * @throws IOException
     */
    public static String store(IrisesFileSystem fs, String originalFilename, boolean generateNewName, InputStream in, boolean override) throws IOException {
        String filename = originalFilename;
        String suffix = null != originalFilename ? FileUtil.getExtension(originalFilename, true) : "";
        if (null == originalFilename || generateNewName) {
            filename = generateFilename() + suffix;
        }

        try {
            StorageUtil.doStore(fs,filename, in, override);
            return filename;
        } catch (IOException e) {
            LOG.warn("文件存储异常: {}", e);
            throw new IOException(e);
        } finally {
            IOUtil.close(in);
        }
    }

    public static void doStore(IrisesFileSystem fs, String fileName, InputStream in, boolean override) throws IOException {
        fs.flow(fileName,in,override);
    }

    /**
     * 生成文件名
     * @return
     */
    private static String generateFilename() {
        byte[] bytes = Bytes.toBytes(System.currentTimeMillis()+ RandomUtil.next(0, Long.MAX_VALUE));
        return HexUtil.encode(bytes);
    }


    /**
     *  图片裁剪与获取
     */

    /**
     * 读取输入流
     * @param id
     * @return
     */
    public static final InputStream readAsStream(String id, boolean tabloid, IrisesFileSystem fs) {
        return tabloid ? doImgOpen(id,fs) : doOpen(id,fs);
    }

    /**
     * 获取文件输入流
     * @param
     * @return
     */
    protected static InputStream doOpen(String fileName, IrisesFileSystem fs) {
        try{
            return fs.open(fileName);
        }catch (Exception e){
            LOG.warn("文件读取异常: {}", e);
            return  null;
        }
    }

    /**
     * 图片裁剪，如果不包含尺寸信息，则返回null
     * @param fileName
     * @param fs
     * @return
     */
    public static InputStream doImgOpen(String fileName, IrisesFileSystem fs) {
        // 可能是需要生成缩略图的请求
        int origNameLength = getOriginalNameLength(fileName);      // 原始图片名称
        if (origNameLength == fileName.length()) {                 // 不包含尺寸信息
            return null;
        }

        String origName = fileName.substring(0, origNameLength);            // 未缩放的文件名
        String suffix = FileUtil.getExtension(origName, true);             // 文件后缀

        // 提取缩略尺寸名字
        String sizeStr;
        if (fileName.endsWith(suffix)) {
            sizeStr = fileName.substring(origNameLength + 1, fileName.length() - suffix.length());
        } else {
            sizeStr = fileName.substring(origNameLength + 1);
        }

        if (null != allowSizeList && allowSizeList.size() > 0 && !allowSizeList.contains(fileName)) {
            LOG.debug("缩略图尺寸不合法: {} {}", origName, fileName);
            return null;
        }

        InputStream in = doOpen(FileUtil.getNameWithoutExtension(origName) + "_" + sizeStr + suffix,fs);
        if (null != in) {       // 如果存在该缩略图，则直接返回
            return in;
        }


        // 获取所缩略图大小
        String[] size = sizeStr.split(SIZE_SEPARATOR);
        if (2 != size.length) {
            LOG.debug("缩略图尺寸不合法: {} {}", origName, fileName);
            return null;
        }
        boolean ratio = true; //是否保持等比
        if(size[1].endsWith("!")) {
            ratio = false;
            size[1] = size[1].substring(0,size[1].length()-1);
        }

        // 读取原始文件
        in = doOpen(origName,fs);
        if (null == in) {
            LOG.debug("源文件不存在: {}", origName);
            return null;
        }

        try {
            int width = Integer.parseInt(size[0]);
            int height = Integer.parseInt(size[1]);
            String thumbnail = getThumbnailName(origName, width, height);
            File tempFile = File.createTempFile("ponly-ils", suffix);
            IOUtil.flow(in, new FileOutputStream(tempFile), true, true);

            File thumb = new File(tempFile.getAbsolutePath() + "." + sizeStr + suffix);
            LOG.debug("生成缩略图 {}", thumbnail);
            // 生成缩略图
            generateThumb(tempFile, thumb, width, height,ratio);

            if (storeThumbnail) {  //是否保存缩略图
                StorageUtil.doStore(fs,thumbnail,new FileInputStream(thumb),true);
            }
            tempFile.deleteOnExit();
            return new FileInputStream(thumb);
        } catch (Exception e) {
            // 如果异常，返回原图
            LOG.warn("自动生成缩略图异常 {}", e);
            return null;
        }
    }

    private static int getOriginalNameLength(String thumbName) {
        int len = thumbName.length();
        char[] chars = thumbName.toCharArray();

        for (int i = 0; i < chars.length - 1; i++) {
            char cur = chars[i];
            char next = chars[i + 1];

            if ('_' == cur && Character.isDigit(next)) {
                len = i;
            }
        }
        return len;
    }


    private static String getThumbnailName(String origName, int w, int h) {
        String suffix = FileUtil.getExtension(origName, false);
        String nameWithoutExtension = FileUtil.getNameWithoutExtension(origName);
        return MessageFormat.format(THUMB_NAME_FMT, nameWithoutExtension, w, h, suffix);
    }

    /**
     * 生成缩略图
     *
     * @param source
     * @param thumb
     * @param width
     * @param height
     */
    private static void generateThumb(File source, File thumb, int width, int height, boolean ratio) {
        try {
            try {
                if (gmIsValid) {
                    IMOperation op = new IMOperation();
//                     op.resize(width, height, "^").gravity("center").extent(width, height).quality(90d);
                    op.resize(width, height, ratio ? "" : "!").gravity("center").quality(90d);
                    op.addImage().addImage();

                    runIMConvertOps(op, source.getAbsolutePath(), thumb.getAbsolutePath());
                    return;
                }
            } catch (Exception e) {
                LOG.warn("GraphicsMagic execute fail: ", e);
            }

            String format = FileUtil.getExtension(thumb.getName(), false);
            BufferedImage orignal = Images.read(source);
            Image result = Images.scale(orignal, width, height, Image.SCALE_FAST | Image.SCALE_SMOOTH);
            Images.write(result, format, thumb);
        } catch (IOException e) {
            LOG.warn("GraphicsMagic execute fail: ", e);
        }
    }


    /**
     * 运行IM4j 命令
     * @param ops
     * @param args
     * @throws InterruptedException
     * @throws IOException
     * @throws IM4JavaException
     */
    private static void runIMConvertOps(IMOps ops, Object... args) throws InterruptedException, IOException, IM4JavaException {
        ConvertCmd cmd = new ConvertCmd(true);
        cmd.run(ops, args);
    }







    private static boolean gmIsValid = false;
    private static boolean cmykSupported = false;

    static {
        try {
            // 测试 graphics magic
            ImageCommand gm = new ImageCommand("gm");
            gm.setOutputConsumer(NullOutputConsumer.INSTANCE);
            gm.run(new Operation().addRawArgs("-help"));

            gmIsValid = true;
            LOG.info("find GM (Graphics Magick)...");
        } catch (Exception e) {
            /* ignore */
        }

        try {
            // 尝试 twelvemonkeys imageio (cmyk jpeg support)
            Thread.currentThread().getContextClassLoader().loadClass("com.twelvemonkeys.imageio.plugins.jpeg.JPEGImageReaderSpi");

            cmykSupported = true;
            LOG.info("find \"twelyemonkeys imageio-jpeg\" plugins ...");
        } catch (ClassNotFoundException ignore) { /* ignore */ }

        if (!gmIsValid && !cmykSupported) {
            LOG.warn("Application is not support CMYK images (JPEG),");
            LOG.warn("because can't find \"twelyemonkeys imageio-jpeg\" plugins or \"GM (Graphics Magick)\",");
            LOG.warn("you can find them here, imageio-jpeg  https://github.com/haraldk/TwelveMonkeys, GM: http://www.graphicsmagick.org");
        }
    }

    private static class NullOutputConsumer implements OutputConsumer {
        public static final NullOutputConsumer INSTANCE = new NullOutputConsumer();

        @Override
        public void consumeOutput(InputStream inputStream) throws IOException {
            inputStream.close();
        }
    }

    /**
     * 文件转码
     * @param rootDir 文件系统根目录
     * @param fileName 需要转码的文件名
     * @return 0:成功
     * @throws Exception
     */
    public static String transcode(String rootDir, String fileName) throws Exception {
        String ffmpegBinPath = "C:/Program Files/ffmpeg/bin/";
        String targetName = fileName.substring(0,fileName.lastIndexOf(".")) + ".mp4";
        String source = new File(rootDir + fileName).getAbsolutePath();
        String target = new File(rootDir + targetName).getAbsolutePath();
        List<String> commend = new ArrayList<>();
        commend.add(ffmpegBinPath + "ffmpeg.exe");
        commend.add("-i");
        commend.add(source);
        commend.add("-s");
        commend.add("320x240");
        commend.add(target);
        ProcessBuilder builder = new ProcessBuilder(commend);
        Process process = builder.start();
        //获取进程的标准输入流
        final InputStream is1 = process.getInputStream();
        //获取进城的错误流
        final InputStream is2 = process.getErrorStream();
        //启动两个线程，一个线程负责读标准输出流，另一个负责读标准错误流，防止卡死
        new Thread(() -> {
            BufferedReader br1 = new BufferedReader(new InputStreamReader(is1));
            try {
                String line1 = null;
                while ((line1 = br1.readLine()) != null) {
                    if (line1 != null){}
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally{
                try {
                    is1.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(() -> {
            BufferedReader br2 = new BufferedReader(new InputStreamReader(is2));
            try {
                String line2 = null ;
                while ((line2 = br2.readLine()) !=  null ) {
                    if (line2 != null){}
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally{
                try {
                    is2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        int code = process.waitFor();
        if(0 != code) {
            throw new RuntimeException("转码失败");
        }
        return targetName;
    }
}
