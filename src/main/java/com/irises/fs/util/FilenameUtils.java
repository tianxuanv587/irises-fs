package com.irises.fs.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class FilenameUtils {
    private static final int NOT_FOUND = -1;
    public static final char EXTENSION_SEPARATOR = '.';
    public static final String EXTENSION_SEPARATOR_STR = Character.toString('.');
    private static final char UNIX_SEPARATOR = '/';
    private static final char WINDOWS_SEPARATOR = '\\';
    private static final char SYSTEM_SEPARATOR;
    private static final char OTHER_SEPARATOR;

    public FilenameUtils() {
    }

    static boolean isSystemWindows() {
        return SYSTEM_SEPARATOR == '\\';
    }

    private static boolean isSeparator(char ch) {
        return ch == '/' || ch == '\\';
    }


    public static String separatorsToUnix(String path) {
        return path != null && path.indexOf(92) != -1 ? path.replace('\\', '/') : path;
    }

    public static String separatorsToWindows(String path) {
        return path != null && path.indexOf(47) != -1 ? path.replace('/', '\\') : path;
    }

    public static String separatorsToSystem(String path) {
        if (path == null) {
            return null;
        } else {
            return isSystemWindows() ? separatorsToWindows(path) : separatorsToUnix(path);
        }
    }


    public static int indexOfLastSeparator(String filename) {
        if (filename == null) {
            return -1;
        } else {
            int lastUnixPos = filename.lastIndexOf(47);
            int lastWindowsPos = filename.lastIndexOf(92);
            return Math.max(lastUnixPos, lastWindowsPos);
        }
    }

    public static int indexOfExtension(String filename) {
        if (filename == null) {
            return -1;
        } else {
            int extensionPos = filename.lastIndexOf(46);
            int lastSeparator = indexOfLastSeparator(filename);
            return lastSeparator > extensionPos ? -1 : extensionPos;
        }
    }

    public static String getName(String filename) {
        if (filename == null) {
            return null;
        } else {
            failIfNullBytePresent(filename);
            int index = indexOfLastSeparator(filename);
            return filename.substring(index + 1);
        }
    }

    private static void failIfNullBytePresent(String path) {
        int len = path.length();

        for(int i = 0; i < len; ++i) {
            if (path.charAt(i) == 0) {
                throw new IllegalArgumentException("Null byte present in file/path name. There are no known legitimate use cases for such data, but several injection attacks may use it");
            }
        }

    }

    public static String getBaseName(String filename) {
        return removeExtension(getName(filename));
    }

    public static String getExtension(String filename) {
        if (filename == null) {
            return null;
        } else {
            int index = indexOfExtension(filename);
            return index == -1 ? "" : filename.substring(index + 1);
        }
    }

    public static String removeExtension(String filename) {
        if (filename == null) {
            return null;
        } else {
            failIfNullBytePresent(filename);
            int index = indexOfExtension(filename);
            return index == -1 ? filename : filename.substring(0, index);
        }
    }

    public static boolean isExtension(String filename, String extension) {
        if (filename == null) {
            return false;
        } else {
            failIfNullBytePresent(filename);
            if (extension != null && !extension.isEmpty()) {
                String fileExt = getExtension(filename);
                return fileExt.equals(extension);
            } else {
                return indexOfExtension(filename) == -1;
            }
        }
    }

    public static boolean isExtension(String filename, String[] extensions) {
        if (filename == null) {
            return false;
        } else {
            failIfNullBytePresent(filename);
            if (extensions != null && extensions.length != 0) {
                String fileExt = getExtension(filename);
                String[] var3 = extensions;
                int var4 = extensions.length;

                for(int var5 = 0; var5 < var4; ++var5) {
                    String extension = var3[var5];
                    if (fileExt.equals(extension)) {
                        return true;
                    }
                }

                return false;
            } else {
                return indexOfExtension(filename) == -1;
            }
        }
    }

    public static boolean isExtension(String filename, Collection<String> extensions) {
        if (filename == null) {
            return false;
        } else {
            failIfNullBytePresent(filename);
            if (extensions != null && !extensions.isEmpty()) {
                String fileExt = getExtension(filename);
                Iterator var3 = extensions.iterator();

                String extension;
                do {
                    if (!var3.hasNext()) {
                        return false;
                    }

                    extension = (String)var3.next();
                } while(!fileExt.equals(extension));

                return true;
            } else {
                return indexOfExtension(filename) == -1;
            }
        }
    }

    static String[] splitOnTokens(String text) {
        if (text.indexOf(63) == -1 && text.indexOf(42) == -1) {
            return new String[]{text};
        } else {
            char[] array = text.toCharArray();
            ArrayList<String> list = new ArrayList();
            StringBuilder buffer = new StringBuilder();
            char prevChar = 0;
            char[] var5 = array;
            int var6 = array.length;

            for(int var7 = 0; var7 < var6; ++var7) {
                char ch = var5[var7];
                if (ch != '?' && ch != '*') {
                    buffer.append(ch);
                } else {
                    if (buffer.length() != 0) {
                        list.add(buffer.toString());
                        buffer.setLength(0);
                    }

                    if (ch == '?') {
                        list.add("?");
                    } else if (prevChar != '*') {
                        list.add("*");
                    }
                }

                prevChar = ch;
            }

            if (buffer.length() != 0) {
                list.add(buffer.toString());
            }

            return (String[])list.toArray(new String[list.size()]);
        }
    }

    static {
        SYSTEM_SEPARATOR = File.separatorChar;
        if (isSystemWindows()) {
            OTHER_SEPARATOR = '/';
        } else {
            OTHER_SEPARATOR = '\\';
        }

    }
}
