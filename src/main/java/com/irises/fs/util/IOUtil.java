//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.irises.fs.util;

import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Enumeration;
import java.util.zip.CRC32;

/**
 * @author Administrator
 */
public abstract class IOUtil {
    protected static final int DEFAULT_BYTE_BUFFER_SIZE = 8192;
    protected static final int DEFUALT_CHAR_BUFFER_SIZE = 100;
    private static final String __LICENSE_DATA = "__NULL__";

    public static void flow(InputStream is, OutputStream os, boolean closeIn, boolean closeOut) throws IOException {
        byte[] buffer = new byte[DEFAULT_BYTE_BUFFER_SIZE];
        flow(is, os, buffer, closeIn, closeOut);
    }

    public static void flow(InputStream is, OutputStream os, byte[] buffer, boolean closeIn, boolean closeOut) throws IOException {
        try {
            int readed;
            while(-1 < (readed = is.read(buffer))) {
                os.write(buffer, 0, readed);
            }

            os.flush();
        } finally {
            if(closeIn) {
                close(is);
            }

            if(closeOut) {
                close(os);
            }

        }
    }

    public static void flow(Reader reader, Writer writer, boolean closeIn, boolean closeOut) throws IOException {
        char[] buffer = new char[DEFUALT_CHAR_BUFFER_SIZE];
        flow(reader, writer, buffer, closeIn, closeOut);
    }

    public static void flow(Reader reader, Writer writer, char[] buffer, boolean closeIn, boolean closeOut) throws IOException {
        try {
            int readed;
            while(-1 < (readed = reader.read(buffer))) {
                writer.write(buffer, 0, readed);
            }

            writer.flush();
        } finally {
            if(closeIn) {
                close(reader);
            }

            if(closeOut) {
                close(writer);
            }

        }
    }

    public static void flow(ReadableByteChannel rbc, WritableByteChannel wbc, boolean closeIn, boolean closeOut) throws IOException {
        flow((ReadableByteChannel)rbc, (WritableByteChannel)wbc, (ByteBuffer)null, closeIn, closeOut);
    }

    public static void flow(ReadableByteChannel rbc, WritableByteChannel wbc, ByteBuffer buffer, boolean closeIn, boolean closeOut) throws IOException {
        if(buffer == null || buffer.capacity() < 1) {
            buffer = ByteBuffer.allocate(8192);
        }

        try {
            while(-1 < rbc.read(buffer)) {
                buffer.flip();
                wbc.write(buffer);
                buffer.clear();
            }
        } finally {
            if(closeIn) {
                close(rbc);
            }

            if(closeOut) {
                close(wbc);
            }

        }

    }

    public static void readFully(InputStream is, byte[] buff, boolean closeIn) throws IOException {
        readFully(is, buff, 0, closeIn);
    }

    public static void readFully(InputStream is, byte[] buff, int offset, boolean closeIn) throws IOException {
        int length = buff.length - offset;
        int actual = read(is, buff, offset, length, closeIn);
        if(actual != length) {
            throw new EOFException("Length to read: " + length + " actual: " + actual);
        }
    }

    public static int read(InputStream is, byte[] buff, int offset, int length, boolean closeIn) throws IOException {
        if(length < 0) {
            throw new IllegalArgumentException("length must be greater than or equal 0");
        } else {
            int remaining;
            int readed;
            for(remaining = length; remaining > 0; remaining -= readed) {
                int location = length - remaining;
                readed = is.read(buff, offset + location, remaining);
                if(-1 == readed) {
                    break;
                }
            }

            if(closeIn) {
                close(is);
            }

            return length - remaining;
        }
    }

    public static byte[] readBytes(InputStream is, boolean closeIn) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
        flow((InputStream)is, (OutputStream)baos, closeIn, true);
        return baos.toByteArray();
    }

    public static String toString(InputStream is, Charset charset, boolean closeIn) throws IOException {
        InputStreamReader reader = charset == null?new InputStreamReader(is):new InputStreamReader(is, charset);
        return toString(reader, closeIn);
    }

    public static String toString(Reader reader, boolean closeIn) throws IOException {
        StringWriter writer = new StringWriter();
        flow((Reader)reader, (Writer)writer, closeIn, true);
        return writer.toString();
    }

    public static String toString(File file) throws IOException {
        return toString(file, Charset.defaultCharset());
    }

    public static String toString(File file, Charset charset) throws IOException {
        return toString(new FileInputStream(file), charset, true);
    }

    public static void write(CharSequence charseq, OutputStream os, Charset charset, boolean closeOut) throws IOException {
        OutputStreamWriter writer = charset == null?new OutputStreamWriter(os):new OutputStreamWriter(os, charset);
        write(charseq, writer, closeOut);
    }

    public static void write(CharSequence charseq, Writer writer, boolean closeOut) throws IOException {
        try {
            writer.write(charseq.toString());
            writer.flush();
        } finally {
            if(closeOut) {
                close(writer);
            }

        }

    }

    public static BufferedInputStream toBuffered(InputStream is) {
        return null != is && !(is instanceof BufferedInputStream)?new BufferedInputStream(is):(BufferedInputStream)is;
    }

    public static BufferedReader toBuffered(Reader reader) {
        return null != reader && !(reader instanceof BufferedReader)?new BufferedReader(reader):(BufferedReader)reader;
    }

    public static void close(Closeable closeable) {
        if(closeable != null) {
            try {
                closeable.close();
            } catch (IOException var2) {
                ;
            }
        }

    }

    private IOUtil() {
    }

    private static String __getMachineId() {
        String hostname = null;
        String ethernet = null;
        InetAddress localhost = null;

        try {
            localhost = InetAddress.getLocalHost();
        } catch (UnknownHostException var11) {
            ;
        }

        try {
            int var8;
            if(localhost == null) {
                hostname = "UNKNOWN";
            } else {
                hostname = localhost.getHostName();
                NetworkInterface ex = NetworkInterface.getByInetAddress(localhost);
                if(!ex.isLoopback() && !ex.isVirtual() && !ex.isPointToPoint() && ex.getHardwareAddress() != null) {
                    byte[] nis = ex.getHardwareAddress();
                    StringBuilder ni = new StringBuilder(nis.length << 1);
                    byte[] var9 = nis;
                    var8 = nis.length;

                    for(int aByte = 0; aByte < var8; ++aByte) {
                        byte bytes = var9[aByte];
                        ni.append(Integer.toHexString(255 & bytes));
                    }

                    ethernet = ni.toString();
                }
            }

            if(ethernet == null) {
                StringBuilder var13 = new StringBuilder();
                Enumeration var14 = NetworkInterface.getNetworkInterfaces();

                label69:
                while(true) {
                    NetworkInterface var15;
                    do {
                        do {
                            do {
                                do {
                                    if(!var14.hasMoreElements()) {
                                        if(1 < var13.length()) {
                                            ethernet = var13.deleteCharAt(var13.length() - 1).toString();
                                        }
                                        break label69;
                                    }

                                    var15 = (NetworkInterface)var14.nextElement();
                                } while(var15.isLoopback());
                            } while(var15.isVirtual());
                        } while(var15.isPointToPoint());
                    } while(var15.getHardwareAddress() == null);

                    byte[] var16 = var15.getHardwareAddress();
                    byte[] var10 = var16;
                    int var18 = var16.length;

                    for(var8 = 0; var8 < var18; ++var8) {
                        byte var17 = var10[var8];
                        var13.append(Integer.toHexString(255 & var17));
                    }

                    var13.append("-");
                }
            }

            if(ethernet == null) {
                throw new IllegalStateException("invalid client");
            } else {
                return Base64.getEncoder().encodeToString(("[P]" + hostname + ":" + ethernet).getBytes(Charset.forName("UTF-8")));
            }
        } catch (SocketException var12) {
            throw new IllegalStateException("");
        }
    }

    private static BigInteger __decodeGroups(String encoded) throws IllegalStateException {
        BigInteger orig = BigInteger.ZERO;
        BigInteger max = BigInteger.valueOf(60466176L);

        int j;
        for(int i = encoded.length(); i >= 0; i = j - 1) {
            j = encoded.lastIndexOf(45, i - 1) + 1;
            String str = encoded.substring(j, i);
            int k = __decodeGroup(str);
            orig = orig.multiply(max);
            orig = orig.add(BigInteger.valueOf((long)k));
        }

        return orig;
    }

    private static int __decodeGroup(String grouped) throws IllegalStateException {
        if(grouped.length() != 5) {
            throw new IllegalStateException();
        } else {
            boolean i = true;
            int j = 0;

            for(int k = grouped.length() - 1; k >= 0; --k) {
                char m = grouped.charAt(k);
                int n;
                if(48 <= m && m <= 57) {
                    n = m - 48;
                } else {
                    if(65 > m || m > 90) {
                        throw new IllegalStateException();
                    }

                    n = m - 65 + 10;
                }

                j *= 36;
                j += n;
            }

            return j;
        }
    }

    private static short __calculateCRC32(String mid, int i, byte[] bytes) {
        CRC32 crc32 = new CRC32();
        int j;
        if(mid != null) {
            for(j = 0; j < mid.length(); ++j) {
                crc32.update(mid.charAt(j));
            }
        }

        crc32.update(i);
        crc32.update(i >> 8);
        crc32.update(i >> 16);
        crc32.update(i >> 24);

        for(j = 0; j < bytes.length - 2; ++j) {
            crc32.update(bytes[j]);
        }

        return (short)((int)crc32.getValue());
    }

    private static String __fetchLicense(String machineId) {
        String licenseUrl = "http://gitool.glanway.com/license";
        HttpURLConnection conn = null;
        BufferedReader reader = null;
        String license = null;

        try {
            try {
                conn = (HttpURLConnection)(new URL(licenseUrl)).openConnection();
                conn.setRequestProperty("X-MACHINE", machineId);
                int responseCode = conn.getResponseCode();
                if(200 == responseCode) {
                    reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                    license = reader.readLine();
                }
            } finally {
                if(reader != null) {
                    reader.close();
                }

                if(conn != null) {
                    conn.disconnect();
                }

            }
        } catch (IOException var10) {
            ;
        }

        return license;
    }
}
