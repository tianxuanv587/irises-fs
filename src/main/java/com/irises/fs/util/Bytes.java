//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.irises.fs.util;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * @author Administrator
 */
public abstract class Bytes {
    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

    public static byte[] toBytes(int integer) {
        byte[] bytes = new byte[]{(byte)(255 & (-16777216 & integer) >> 24), (byte)(255 & (16711680 & integer) >> 16), (byte)(255 & ('\uff00' & integer) >> 8), (byte)(255 & integer)};
        return bytes;
    }

    public static byte[] toBytes(long l) {
        byte[] bytes = new byte[]{(byte)((int)(255L & (-72057594037927936L & l) >> 56)), (byte)((int)(255L & (71776119061217280L & l) >> 48)), (byte)((int)(255L & (280375465082880L & l) >> 40)), (byte)((int)(255L & (1095216660480L & l) >> 32)), (byte)((int)(255L & (4278190080L & l) >> 24)), (byte)((int)(255L & (16711680L & l) >> 16)), (byte)((int)(255L & (65280L & l) >> 8)), (byte)((int)(255L & l))};
        return bytes;
    }

    public static byte[] toBytes(char[] chars) {
        return toBytes(chars, 0, chars.length);
    }

    public static byte[] toBytes(char[] chars, int offset, int len) {
        return toBytes(String.valueOf(chars, offset, len));
    }

    public static byte[] toBytes(String text) {
        return toBytes(text, DEFAULT_CHARSET);
    }

    public static byte[] toBytes(String text, Charset charset) {
        return text.getBytes(charset);
    }

    public static int toInt(byte[] bytes) {
        if(bytes.length > 4) {
            throw new IllegalArgumentException("bytes length must not be greater than 4");
        } else {
            int shift = 0;
            int result = 0;

            for(int i = bytes.length - 1; i >= 0; shift += 8) {
                result |= (bytes[i] & 255) << shift;
                --i;
            }

            return result;
        }
    }

    public static long toLong(byte[] bytes) {
        if(bytes.length > 8) {
            throw new IllegalArgumentException("bytes length must not be greater than 8");
        } else {
            int shift = 0;
            long result = 0L;

            for(int i = bytes.length - 1; i >= 0; shift += 8) {
                result |= (long)(bytes[i] & 255) << shift;
                --i;
            }

            return result;
        }
    }

    public static String toString(byte[] bytes) {
        return toString(bytes, DEFAULT_CHARSET);
    }

    public static String toString(byte[] bytes, Charset charset) {
        return new String(bytes, charset);
    }

    public static char[] toChars(byte[] bytes) {
        return toChars(bytes, DEFAULT_CHARSET);
    }

    public static char[] toChars(byte[] bytes, Charset charset) {
        return toString(bytes, charset).toCharArray();
    }

    public static byte[] toBytes(Object o) {
        if(o == null) {
            String msg = "Argument for String conversion cannot be null.";
            throw new IllegalArgumentException(msg);
        } else {
            return o instanceof Integer?toBytes(((Integer)o).intValue()):(o instanceof Long?toBytes(((Long)o).longValue()):(o instanceof byte[]?(byte[])((byte[])o):(o instanceof char[]?toBytes((char[])((char[])o)):(o instanceof String?toBytes((String)o):(o instanceof File?toBytes((File)o):(o instanceof URL?toBytes((URL)o):(o instanceof InputStream?toBytes((InputStream)o):objectToBytes(o))))))));
        }
    }

    private static byte[] objectToBytes(Object o) {
        throw new UnsupportedOperationException("unsupported object cast to bytes");
    }

    protected static byte[] toBytes(File file) {
        if(file == null) {
            throw new IllegalArgumentException("File argument cannot be null.");
        } else {
            try {
                return toBytes((InputStream)(new FileInputStream(file)));
            } catch (FileNotFoundException var2) {
                return null;
            }
        }
    }

    protected static byte[] toBytes(URL url) {
        if(url == null) {
            throw new IllegalArgumentException("Url argument cannot be null.");
        } else {
            try {
                return toBytes(url.openStream());
            } catch (IOException var2) {
                return null;
            }
        }
    }

    protected static byte[] toBytes(InputStream in) {
        if(in == null) {
            throw new IllegalArgumentException("InputStream argument cannot be null.");
        } else {
            try {
                return IOUtil.readBytes(in, true);
            } catch (IOException var2) {
                return null;
            }
        }
    }

    private Bytes() {
    }
}
