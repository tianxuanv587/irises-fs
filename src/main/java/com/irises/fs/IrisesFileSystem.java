package com.irises.fs;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * 文件系统
 * <p/>
 * 该接口定义一个与具体实现无关的文件系统接口
 *
 * @author tianxuan
 */
public interface IrisesFileSystem {

    /**
     * 打开给定路径的文件, 如果给定路径不存在返回null, 如果不是文件将抛出异常
     * <p/>
     * 注: 关闭输入流时才会关闭相关资源, 因此读取完毕后务必关闭输入流
     *
     * @param file 文件路径
     * @return 文件输入流或null
     * @throws FileSystemException
     */
    InputStream open(String file);

    /**
     * 获取指向目标文件file对象
     * @param file
     * @return
     * @throws FileSystemException
     */
    File getFile(String file);

    String getChrootDirectory();

    /**
     * 存储文件
     * @param file
     * @param override
     * @throws FileSystemException
     */
    void flow(String file, InputStream in, boolean override) throws IOException;


}
