package com.irises;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Demo {

    public static void main(String[] args) throws IOException, InterruptedException {
        String ffmpegBinPath = "C:\\Program Files\\ffmpeg\\bin\\";
        List<String> commend = new ArrayList<>();
        commend.add(ffmpegBinPath + "ffmpeg.exe");
        commend.add("-i");
        commend.add("D:/aaa.3gp");
        commend.add("-s");
        commend.add("320x240");
        commend.add("D:/aaa.mp4");
        ProcessBuilder builder = new ProcessBuilder(commend);
        Process process = builder.start();
        //获取进程的标准输入流
        final InputStream is1 = process.getInputStream();
        //获取进城的错误流
        final InputStream is2 = process.getErrorStream();
        //启动两个线程，一个线程负责读标准输出流，另一个负责读标准错误流，防止卡死
        new Thread(() -> {
            BufferedReader br1 = new BufferedReader(new InputStreamReader(is1));
            try {
                String line1 = null;
                while ((line1 = br1.readLine()) != null) {
                    if (line1 != null){}
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally{
                try {
                    is1.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(() -> {
            BufferedReader br2 = new BufferedReader(new InputStreamReader(is2));
            try {
                String line2 = null ;
                while ((line2 = br2.readLine()) !=  null ) {
                    if (line2 != null){}
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally{
                try {
                    is2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        int code = process.waitFor();
        System.out.println(code);
    }
}